
<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
        <title><?php echo $this->title ?></title>
        <meta name="description" content="<?php echo $this->description ?>">
        <meta name="keywords" content="<?php echo $this->keywords ?>">
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico"  rel="shortcut icon" type="image/x-icon" />
		<?php Yii::app()->clientScript->registerCoreScript('jquery');?>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<?php Yii::app()->clientScript->registerPackage('main'); ?>
	</head>
	<body>
		<header>
			<div class="container">
				<nav class="main-navigation">
					<?php if (Yii::app()->user->isGuest){?>
						<ul>
							<li  id="login-link"><a data-toggle="modal" href="#modal_01">Вход</a></li>
							<li><a data-toggle="modal" href="#modal_02">Регистрация</a></li>

						</ul>
					<?php }	 else {?>
						<ul>
							<?php  if(Yii::app()->user->checkAccess(User::ROlE_ADMIN)){
								echo '<li>' . CHtml::link( t('Admin Panel'), array('/site/meta')) . '</li>';
							}?>

							<li>
								<a href="<?php echo $this->createUrl('accounts/logout')?>">
								<?php echo '(' . Yii::app()->user->getFullName(). ')';?>
								Выход
								</a>
							</li>

						</ul>
					<?php }?>
				</nav>
				<div class="logo">
					<a href="/">
					<?php if(!empty($this->meta)  AND !empty($this->meta->logo)) {?>
						<img src="<?php echo $this->meta->logo?>" alt="" class="logo_image">
					<?php }?>
					</a>
				</div>
			</div>
		</header>

        <?php echo $content; ?>

        <div class="container">
            <div class="partners-row col-xs-12">
                <ul class="partners-row-list">
                    <?php foreach ($this->partners as $partner): ?>
                    <li class="col-xs-2">
                        <a href="<?php echo $partner->link ?>"><img src="<?php echo $partner->image ?>" alt="<?php echo $partner->name ?>"></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

		<footer>
			<div class="container">
				<div class="col-xs-12">
					<div class="clearfix footer-rows">
						<div class="col-xs-3">
                            <?php echo $this->contact1 ?>
						</div>
						<div class="col-xs-3">
                            <?php echo $this->contact2 ?>
						</div>
						<div class="col-xs-3">
                            <?php echo $this->contact3 ?>
						</div>
						<div class="col-xs-3">
							<?php if($this->meta->vk): ?>
								<a title="vk" class="social-vk social-icons" href="<?php echo $this->meta->vk ?>"></a>
							<?php endif; ?>

							<?php if($this->meta->facebook): ?>
								<a title="facebook" class="social-facebook social-icons" href="<?php echo $this->meta->facebook ?>"></a>
							<?php endif; ?>

							<?php if($this->meta->google): ?>
								<a title="google+" class="social-google social-icons" href="<?php echo $this->meta->google ?>"></a>
							<?php endif; ?>


							<?php if($this->meta->twitter): ?>
								<a title="twitter" class="social-twitter social-icons" href="<?php echo $this->meta->twitter ?>"></a>
                            <?php endif; ?>


							<?php if($this->meta->youtube): ?>
								<a title="youtube" class="social-youtube social-icons" href="<?php echo $this->meta->youtube ?>"></a>
                            <?php endif; ?>

						</div>
					</div>
					<div class="col-xs-12">
						<div class="made-by"><p><a target="_blank" href="http://futurity.pro">Сделано в Futurity</a></p></div>
						<div class="copyrights"><p>© 2013 SENDING MEDIA GROUP All rights reserved.</p>
						</div>
					</div>
				</div>
			</div>
		</footer>


	</body>
</html>




<?php
if (Yii::app()->controller->id == 'contest') {
	if ( Yii::app()->user->isGuest  ) {
		$this->renderPartial('//layouts/modal/login_form' 	 , array('model' => $this->model));
		$this->renderPartial('//layouts/modal/register_form' , array('user' => $this->user));
	} else {
		 echo $this->renderPartial('//layouts/modal/send_remix', array('user' => $this->user));
	}

	// show upload form after user login if unlogged user click to upload remix button
	if(Yii::app()->user->isGuest) {
		Yii::app()->clientScript->registerScript('show-popup', "
			var url ='" . CController::createUrl("/accounts/ShowPopup") . "';
			$('.sound-upload').click(function(){
				$.ajax({
					url:  url,
					'data' : {'method' : 'set'}
				})
			});
		");
	 }
	if ( !empty (Yii::app()->session['showPopup']) ){
		Yii::app()->clientScript->registerScript('show-popup', "
			var url ='" . CController::createUrl("/accounts/ShowPopup") . "';
			$('.sound-upload').click();
			$.ajax({
				url:  url,
				'data' : {'method' : 'remove'}
			});
		");
	}
}?>