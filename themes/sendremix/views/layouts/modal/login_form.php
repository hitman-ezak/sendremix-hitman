<div class="modal fade" id="modal_01" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'action'=> $this->createUrl('/accounts/login'),
		'enableAjaxValidation' => TRUE,
		'method' => 'POST',
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnChange'=>true,
			'validateOnType'=>false,
		),
	)); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<ul class="login-registration">
					<li><a href="#modal_01" data-toggle="modal">Вход</a></li>
					<li><a href="#modal_02" data-toggle="modal" >Регистрация</a></li>
				</ul>

				<?php Yii::app()->eauth->renderWidget(); ?>

				<p class="popup-sep-title"><span>Или введите e-mail и пароль</span></p>
				<div class="email-enter clearfix">
					<label class="login-panel">
						<i></i>
						<?php echo $form->textField($model,'username', array('placeholder'=>'Логин','type'=>'email')); ?>
						<?php echo $form->error($model,'username'); ?>
					</label>

					<label class="password-panel">
						<i></i>
						<?php echo $form->passwordField($model,'password', array('placeholder'=>'Пароль')); ?>
						<?php echo $form->error($model,'password'); ?>
					</label>
				</div>
				<?php echo CHtml::link(t('Forgot Password?'), array('/accounts/forgotPassword'), array('class'=>'forget-pass')); ?>
				<label class="rememb-password" for="forget">
					<?php echo $form->checkbox($model,'rememberMe', array('id'=>'forget')); ?>
					<?php echo $form->hiddenField($model,'showPopup'); ?>
					Запомнить меня
				</label>
			</div>
			<div class="modal-footer">
				<button type="submit"class="btn enter-btn" type="submit">Войти<i></i></button>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>