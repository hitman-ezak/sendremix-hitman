<div class="modal fade" id="modal-send-remix" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'=>'remix-form',
		'action'=> $this->createUrl('/contest/CreateRemix'),
		'enableAjaxValidation' => TRUE,
		'method' => 'POST',
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnChange'=>true,
			'validateOnType'=>false,
		),
	)); ?>
	<div class="modal-dialog modal-registration">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="email-enter clearfix">
					<label>
						<?php echo $form->textField($user,'first_name', array('placeholder'=>'Имя')); ?>
						<?php echo $form->error($user,'first_name'); ?>
					</label>

					<label>
						<?php echo $form->textField($user,'last_name', array('placeholder'=>'Фамилия')); ?>
						<?php echo $form->error($user,'last_name'); ?>
					</label>

					<label>
						<?php echo $form->textField($user,'second_name', array('placeholder'=>'Отчество')); ?>
						<?php echo $form->error($user,'second_name'); ?>
					</label>
					<label>
						<?php echo $form->textField($user,'website', array('placeholder'=>'Веб-сайт')); ?>
						<?php echo $form->error($user,'website'); ?>
					</label>

					<label>
						<?php echo $form->textField($user,'city', array('placeholder'=>'Город')); ?>
						<?php echo $form->error($user,'city'); ?>
					</label>

					<label>
						<?php echo $form->textField($user,'country', array('placeholder'=>'Страна')); ?>
						<?php echo $form->error($user,'country'); ?>
					</label>

					<label>
						<?php echo $form->textField($user,'phone', array('placeholder'=>'Телефон')); ?>
						<?php echo $form->error($user,'phone'); ?>
					</label>

					<label>
						<?php echo $form->textField($user,'email', array('placeholder'=>'e-mail')); ?>
						<?php echo $form->error($user,'email'); ?>
					</label>
				</div>
				<fieldset class="rememb-password">
					<?php echo $form->HiddenField($user, 'contest_id' );?>
					<?php echo $form->HiddenField($user, 'acceptRules' );?>
					<input type="checkbox" id="rules">
					Я принимаю условия <a href="/rules">пользовательского соглашения</a>
					<?php echo $form->error($user,'acceptRules'); ?>
				</fieldset>
			</div>

			<div class="modal-footer">
				<?php echo $form->HiddenField($user,'file'); ?>
				<?php echo $form->error($user,'file'); ?>
				<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'uploadPhoto')); ?>
				<?php
				$this->widget('ext.EAjaxUpload.EAjaxUpload',
					array(
						'id' => 'uploadFile',

						'config' => array(
							'action' => Yii::app()->createUrl('/contest/UploadRemix'),
							'allowedExtensions' => array("mp3", "wav", "ogg"),
							'sizeLimit' => 20*1024*1024,
							'minSizeLimit' => 1,
							'onComplete'=>"js:function(id, fileName, responseJSON){
								$('#photo').attr('src', '/uploads/'+responseJSON.filename);
								$('#User_file').val(responseJSON.filename);
								//$.post('".Yii::app()->createUrl('/accounts/UploadRemix')."', {filename: '/uploads/' + responseJSON.filename});
							}"
							)
						)
					);
				?>

				<?php $this->endWidget(); ?>
				<button type="submit"class="btn enter-btn" type="submit">Отправить<i></i></button>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>

<script>
	$(function(){
		$("#rules").click(function(e){
			if($(this).val() == "1"){
				$(this).val("0");
				$("#User_acceptRules").val("0");
			}else{
				$(this).val("1");
				$("#User_acceptRules").val("1");
			}
		});
	})
</script>