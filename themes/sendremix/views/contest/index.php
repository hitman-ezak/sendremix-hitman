<div id="mainSlider" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<?php foreach ($models as $id => $model): ?>
			<li data-target="#mainSlider" data-slide-to="<?php echo $id ?>"<?php if ($id == 0) { ?> class="active"<?php } ?>></li>
		<?php endforeach; ?>
	</ol>
	<div class="carousel-inner">
		<?php foreach ($models as $id => $model): ?>
			<div class="item <?php if ($id == 0) { ?>active<?php } ?>">
				<?php if($model->slide):?>
					<img class="carousel-img-bg" src="<?php echo $model->slide; ?>" alt="<?php echo $model->name; ?>">
				<?php else: ?>
					<img class="carousel-img-bg" src="/images/slide.png" alt="<?php echo $model->name; ?>">
				<?php endif; ?>
				<div class="container">
					<div class="carousel-caption">
						<h1><a href="<?php echo $this->createUrl('/contest/view', array('id'=>$model->id)) ?>"><?php echo $model->name; ?></a></h1>

						<div class="carousel-desc">
							<p><?php echo $model->description; ?></p>
						</div>
						<?php
							$dl_link = '';
							if(Yii::app()->user->isGuest):
								$dl_link = '#modal_01';
							  else:
								$dl_link = $model->downloads?>
							  <script type="text/javascript">
								  $('.sound-download').click(function(){
									  $.ajax({
										  url: '/site/DownloadStat',
										  data: {'contest' : $(this).data('contest')}
									  })
								  });
							  </script>
						<?php endif;?>

							<a href="<?php echo $model->downloads; ?>"
							   href="<?php echo $dl_link ; ?>
							   data-contest="<?php echo $model->id?>"
							   class="sound-btn-small sound-download visible-xs" data-contest="<?php echo $model->id?>">
								<i></i>
							</a>
							<a data-toggle="modal" href="<?php echo $dl_link ; ?>"
							   class="hidden-xs sound-btn sound-download"
							   data-contest="<?php echo $model->id?>">
								Скачать материалы<i></i>
							</a>


						<a href="<?php echo $modal_id ?>"
						   data-contest="<?php echo $model->id?>"
						   data-toggle="modal"
						   class="hidden-xs sound-btn sound-upload">
							Отправить ремикс<i></i>
						</a>

						<a href="<?php echo $modal_id?>" data-toggle="modal" class="visible-xs sound-btn-small sound-upload">
							<i></i>
						</a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<a class="slider-arrows arrow-left" href="#mainSlider" data-slide="prev"></a>
	<a class="slider-arrows arrow-right" href="#mainSlider" data-slide="next"></a>
</div><!-- /.carousel -->
<div class="content">
	<h1>Конкурсы</h1>

	<div class="gray-block songs-slider">
		<div class="container">
			<div id="songSlider" class="carousel slide col-xs-12" data-ride="carousel">
				<!-- Indicators -->
				<div class="carousel-inner">
					<div class="item active">
						<ul class="song-row">
							<?php foreach ($models as $id => $model): ?>
								<li class="song-item">
									<?php if($model->cover):?>
										<img class="carousel-img-bg" src="<?php echo $model->cover; ?>" alt="<?php echo $model->name; ?>">
									<?php else: ?>
										<img class="carousel-img-bg" src="/images/cover.png" alt="<?php echo $model->name; ?>">
									<?php endif; ?>

									<?php $url = $this->createUrl('/contest/view', array('id'=>$model->id))?>
									<div class="song-desc"  onclick="window.location.href='<?php echo $url?>'">
										<h2 class="song-desc-title"><?php echo $model->name; ?></h2>
										<span class="song-desc-autor"><?php echo $model->author; ?></span>

										<div class="song-desc-toggle">
              								<span class="song-desc-text"><p><?//php echo $model->description; ?></p>
              								<a class="view-more" href="<?php echo $url ?>">Перейти</a>
             								</span>
										</div>
									</div>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<a class="slider-arrows arrow-left" href="#songSlider" data-slide="prev"></a>
				<a class="slider-arrows arrow-right" href="#songSlider" data-slide="next"></a>
			</div>
			<!-- /.carousel -->
		</div>
	</div>
	<h1><span>Про</span> Send Remix</h1>

	<div class="gray-block about-us">

		<div class="col-xs-5 about-label-text">
			<?php echo $meta->info; ?>
		</div>

		<button class="btn btn-blue">О проекте</button>
	</div>

</div>

