<?php Yii::app()->clientScript->registerPackage('scroll-to'); ?>
<div id="mainSlider" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#mainSlider" data-slide-to="0" class="active"></li>
        <li data-target="#mainSlider" data-slide-to="1"></li>
        <li data-target="#mainSlider" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <?php if($model->slide):?>
                <img class="carousel-img-bg" src="<?php echo $model->slide; ?>" alt="<?php echo $model->name; ?>">
            <?php else: ?>
                <img class="carousel-img-bg" src="/images/slide.png" alt="<?php echo $model->name; ?>">
            <?php endif; ?>
            <div class="container">
                <div class="carousel-caption">
                    <h1><a href=""><?php echo $model->name ?></a></h1>
                    <div class="carousel-desc">
                        <p><?php echo $model->description ?></p><br>
                    </div>
					<?php
					$dl_link = '';
					if(Yii::app()->user->isGuest):
						$dl_link = '#modal_01';
					else:
						$dl_link = $model->downloads?>
						<script type="text/javascript">
							$(function(){
								$('.sound-download').click(function(){
									$.ajax({
										url: '/site/DownloadStat',
										data: {'contest' : $(this).data('contest')}
									})
								});

							});

						</script>
					<?php endif;?>

						<a data-toggle="modal" href="<?php echo $dl_link ?>" data-contest="<?php echo $model->id?>"
						   class="sound-btn sound-download hidden-xs" data-contest="<?php echo $model->id?>">
							Скачать материалы <i></i>
						</a>

						<a href="<?php echo $dl_link ?>" data-contest="<?php echo $model->id?>"
						   class="sound-btn-small sound-download visible-xs" data-contest="<?php echo $model->id?>">
							<i></i>
						</a>
                    <
                    <a href="<?php echo $modal_id?>" data-toggle="modal" class="hidden-xs sound-btn sound-upload">Отправить ремикс <i></i></a>
                    <a href="<?php echo $modal_id?>" data-toggle="modal" class="visible-xs sound-btn-small sound-upload"><i></i></a>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.carousel -->
<div class="content">
    <div class="participant-item">
        <div class="participant-info">
            <div class="container">
                <div class="col-xs-12">
                    <table class="participant-table col-xs-7">
                        <tr class="participant-main-info">
                            <td><p><?php echo $model->author ?></p></td>
                            <td>
                                <?php if($model->web): ?>
                                <a href="<?php echo $model->web ?>" class="participant-website"><?php echo $model->web ?></a>
                                <?php endif; ?>
                                <?php if($model->vk): ?>
                                <a href="<?php echo $model->vk ?>" class="socnet-ico socnet-vk"></a>
                                <?php endif; ?>
                                <?php if($model->facebook): ?>
                                <a href="<?php echo $model->facebook ?>" class="socnet-ico socnet-fb"></a>
                                <?php endif; ?>
                                <?php if($model->twitter): ?>
                                <a href="<?php echo $model->twitter ?>" class="socnet-ico socnet-tw"></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Дата конкурса<p></td>
                            <td><p><?php echo $model->date_from ?> &mdash; <?php echo $model->date_to ?></p></td>
                        </tr>
                        <tr>
                            <td>
                                <p>Приз</p>
                                <span class="ico-prize"></span>
                            </td>
                            <td><p><?php echo $model->prize ?></p></td>
                        </tr>
                    </table>
                </div>
            </div>

			<?php if(!empty($model->title)) {?>
				<h1><?php echo $model->title ?></h1>
			<?php }?>

			<div class="gray-block about-participant">
                <div class="container">
                    <div class="col-xs-7 about-participant-text">
                        <p><span><?php echo $model->description_under ?></span></p>
                    </div>
                </div>
            </div>
        </div>
        <?php if($model->audio): ?>
        <div class="participant-audio">
            <?php echo $model->audio ?>
        </div>
        <?php endif; ?>
        <?php if($model->video): ?>
        <div class="participant-video">
            <div class="video-container" >
                <?php echo $model->video ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="btn-navigation">
        <div class="container">
            <ul>
                <li><button class="btn sound-btn" id="prizes"><?php echo $meta->title_prizes?></button></li>
                <li><button class="btn sound-btn btn-requirements" id="requirements"><?php echo $meta->title_requirements?></button></li>
                <li><button class="btn sound-btn" id="about"><?php echo $meta->title_about?></button></li>
            </ul>
        </div>
    </div>
    <h1><?php echo $meta->title_prizes?></h1>
    <div class="gray-block prizes">
        <div class="col-xs-12 container">
                <p><?php echo $model->more_prize ?></p>
        </div>
    </div>
    <h1><?php echo $meta->title_requirements?></h1>
    <div class="gray-block requirements">
        <div class="col-xs-12 container">
        	<p><?php echo $model->more_requirements ?></p>
        </div>
    </div>
    <h1><?php echo $meta->title_about?></h1>
    <div class="gray-block about-label">
        <div class="col-xs-12 container">
            <div class="col-xs-5 about-label-text">
                <p><?php echo $model->more_label ?></p>
            </div>
        </div>
    </div>
	<a href="" val></a>
    <div class="comp-item-btns">

        <a href="<?php echo $dl_link ?>" class="sound-btn sound-download"  data-contest="<?php echo $model->id?>" data-toggle="modal">Скачать Материалы <i></i></a>
        <a href="<?php echo $modal_id?>" class="sound-btn sound-upload" data-contest="<?php echo $model->id?>"  data-toggle="modal">Отправить Ремикс <i></i></a>
    </div>
</div>
<script>
	$(function(){
		$('#about').click(function(){
			$.scrollTo('.about-label', 1000);
		});
		$('#prizes').click(function(){
			$.scrollTo('.prizes', 1000);
		});


		$('#requirements').click(function(){
			$.scrollTo('.requirements', 1000);
		});

	})
</script>