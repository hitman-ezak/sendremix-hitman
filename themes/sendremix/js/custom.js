$(function(){

  $('#mainSlider, #songSlider').carousel({
    interval: false,
    pause: "hover",
    wrap: true
  });

    $('.sound-upload').click(function() {
        $('#User_contest_id').val($(this).data('contest'));
    });



  $('#uploadPhoto').removeClass('modal');
  $('#uploadPhoto').removeClass('hide');
  $('#uploadPhoto').removeClass('fade');

  $('.song-item').hover(function(){
    $(this).find('.song-desc-toggle').stop().slideToggle();
  });

});