<div class="row lp">
    <div class="span12">

        <div class="center">
            <h2>Verwalten Sie Ihre persönlichen Finanzen</h2>
            <p class="big grey">Alles auf einer Seite</p>

            <!-- Image -->

            <div class="lp-image">
                <img src="<?php echo Yii::app()->baseUrl; ?>/images/finance.png">
            </div>

            <!-- Button -->

            <div class="button">
                <a href="<?php echo $this->createUrl('/cabinet/accounts/register') ?>">Versuchen</a>
            </div>

            <div class="border"></div>

        </div>

        <div class="row">
            <div class="span6">

                <h4>Konten</h4>
                <p>Willst du Geld Betrag auf Ihre Konten wissen Monat? Sie können den Status der Konten in jeder beliebigen Tag wissen Vergangenheit oder Zukunft</p>

                <hr>

                <h4>Budget</h4>
                <p>Vergessen Sie über die wichtigsten Prepaid? Das Budget für das Jahr erlaubt es, die Menge für verschiedene Bedürfnisse zu differenzieren und die Überwachung ihrer täglich ohne Angabe von Casual Shopping verschwenden das Geld</p>

                <hr>
            </div>

            <div class="span6">

                <h4>Protokoll</h4>
                <p>Eine Woche verging und Gehalt weg? Halten tägliches Protokoll von Aufwendungen und Erträgen wird verfolgen die Bewegungen der einzelnen Fahrzeuge und immer bewusst sein, wann und wie viel ausgegeben wurde</p>

                <hr>

                <h4>Korrekturen</h4>
                <p>Es gibt keine Möglichkeit, jeden Tag log zu halten? Funktion <strong>Konto bestätigen</strong> können Sie Anpassungen an das Abrechnungssystem so stellen, dass die relevanten Daten bleiben</p>

                <hr>
            </div>

        </div>

    </div>
</div>

<aside class="span4">
    <div class="fb-like-box" data-href="https://www.facebook.com/434059270043272" data-width="292" data-height="400" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
</aside>
<aside class="span4">
    <a class="twitter-timeline" href="https://twitter.com/search?q=%23cabinet_de" data-widget-id="366255287725850626">Tweets #cabinet_de</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</aside>