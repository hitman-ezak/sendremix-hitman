<div class="row lp">
    <div class="span12">

        <div class="center">
            <h2>Personal finance management</h2>
            <p class="big grey">All in one page</p>

            <!-- Image -->

            <div class="lp-image">
                <img src="<?php echo Yii::app()->baseUrl; ?>/images/finance.png">
            </div>

            <!-- Button -->

            <div class="button">
                <a href="<?php echo $this->createUrl('/cabinet/accounts/register') ?>">Try</a>
            </div>

            <div class="border"></div>

        </div>

        <div class="row">
            <div class="span6">

                <h4>Accounts</h4>
                <p>Want to know money amount on your accounts month ago? You can know the status of accounts in any day of past or future</p>

                <hr>

                <h4>Budget</h4>
                <p>Forget about the important prepay? The budget for the year allows to differentiate the amount for various needs and monitor their every day without giving a casual shopping squander all the money</p>

                <hr>
            </div>

            <div class="span6">

                <h4>Log</h4>
                <p>A week passed, and salary gone? Keep daily log of expenses and revenues will track the movements of individual vehicles and always be aware of when and how much was spent</p>

                <hr>

                <h4>Corrections</h4>
                <p>There is no way to keep every day log? Function <strong>Verify account</strong> allows you to make adjustments to the accounting system so that the data remain relevant</p>

                <hr>
            </div>

        </div>

    </div>
</div>

<aside class="span4">
    <div class="fb-like-box" data-href="https://www.facebook.com/428565110591304" data-width="292" data-height="400" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
</aside>
<aside class="span4">
    <a class="twitter-timeline" href="https://twitter.com/search?q=%23cabinet_en" data-widget-id="366255025858695168">Tweets about "#cabinet_en"</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</aside>