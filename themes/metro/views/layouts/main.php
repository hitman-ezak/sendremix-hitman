<!DOCTYPE html>
<html lang="<?=t('en')?>" class="csstransforms csstransforms3d csstransitions js cssanimations csstransitions">
<head>
    <meta charset="utf-8">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?=t('Personal finance management')?>">
    <meta name="keywords" content="<?=t('Cabinet')?>">
    <meta name="author" content="<?=t('Aleksey Razbakov')?>">

    <meta property='og:locale' content='<?=t('en_US')?>'>
    <meta property='og:type' content='article'>
    <meta property='og:title' content='<?=t('Personal finance management')?>'>
    <meta property='og:url' content='http://cabinet.io/'>
    <meta property='og:site_name' content='<?=t('Cabinet')?>'>
    <meta property='og:image' content='http://cabinet.io/images/icon.png'>

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/bootstrap.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/font-awesome.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/prettyPhoto.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/slider.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/flexslider.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/blue.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/bootstrap-responsive.css">

    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/html5shim.js"></script>
    <![endif]-->

    <?php if(Yii::app()->language == 'ru'): ?>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?97"></script>

    <script type="text/javascript">
        VK.init({apiId: 3800793, onlyWidgets: true});
    </script>
    <?php endif; ?>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/<?=t('en_US')?>/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Header Starts -->
<header>
    <div class="container">
        <div class="row">
            <div class="span6">
                <div class="logo">
                    <h1><a href="<?php echo $this->createUrl('/') ?>"><img style="height: 50px"
                                         src="/images/cabinet-<?=t('en') ?>.png"></a></h1>

                    <div class="hmeta"><?=t('Personal finance management')?></div>
                </div>
            </div>
            <div class="language span3">
                <?php
                foreach (array('en', 'de', 'ru') as $lang) {
                    echo CHtml::link('<span class="'.$lang.'">'.$lang.'</span>', array(
                        (isset(Yii::app()->controller->module)?'/'.Yii::app()->controller->module->id:'').
                        '/'.Yii::app()->controller->id.
                        '/'.Yii::app()->controller->getAction()->id,
                        Yii::app()->urlManager->langParam => $lang), array(
                        'class' => ((Yii::app()->language == $lang) ? 'active' : ''),
                    )).' ';
                }
                ?>
            </div>
            <div class="span3">
                <div class="form" style="width: 100% !important; max-width: 100%; text-align: right; display: block;">
                    <a href="<?php echo $this->createUrl('/cabinet/accounts/login') ?>"><?=t('Login') ?></a> <?=t('or') ?>
                    <a href="<?php echo $this->createUrl('/cabinet/accounts/register') ?>"><?=t('Registration') ?></a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Navigation bar starts -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span>Menu</span>
            </a>

            <div class="nav-collapse collapse">
                <?php $this->widget('bootstrap.widgets.TbMenu', array(
                    'items' => array(
                        array(
                            'label' => t('Home'),
                            'url' => array('/site/index'),
                        ),
                        array(
                            'label' => t('About'),
                            'url' => array('/site/about'),
                        ),
//                        array(
//                            'label'=>'Блог',
//                            'url'=>'/blog',
//                        ),
                    )
                )); ?>
            </div>
        </div>
    </div>
</div>

<!-- Navigation bar ends -->

<div class="content">
    <div class="container">
        <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true, // display a larger alert block?
        'fade' => true, // use transitions?
        'closeText' => '&times;', // close link text - if set to false, no close link is displayed
        'alerts' => array( // configurations per alert type
            'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'), // success, info, warning, error or danger
        ),
    )); ?>

        <?php echo $content; ?>
        <div class="clear"></div>
        <div class="border"></div>

        <div style="text-align: center; margin-top: 20px;">
            <?php if(Yii::app()->language == 'ru'): ?>
            <span id="vk_like"></span>
            <script type="text/javascript">
                VK.Widgets.Like("vk_like", {type: "button", verb: 2});
            </script>
            <?php endif; ?>
            <div class="fb-like" data-href="http://cabinet.io/" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
            <a href="https://twitter.com/intent/tweet?button_hashtag=cabinet_<?=t('en')?>" class="twitter-hashtag-button" data-lang="<?=t('en')?>" data-related="cabinetio" data-url="http://cabinet.io/<?=t('en')?>"><?=t('Tweet')?> #cabinet_<?=t('en')?></a>
            <g:plusone></g:plusone>
            <a target="_blank" class="surfinbird__like_button" data-surf-config="{'layout': 'common-blue', 'width': '120', 'height': '21'}"><?=t('Serf')?></a>
            <a href="http://pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" ><img src="http://assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
            <script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            <!-- Put this script tag after all buttons -->
            <script type="text/javascript" charset="UTF-8" src="http://surfingbird.ru/share/share.min.js"></script>
            <script type="text/javascript">
                window.___gcfg = {lang: '<?=t('en')?>'};

                (function() {
                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                    po.src = 'https://apis.google.com/js/plusone.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                })();
            </script>
        </div>
    </div>

    <div class="social-links">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <p class="big">
                        <span><?=t('Follow us') ?></span>
                        <?php if(Yii::app()->language == 'ru'): ?>
                        <a target="_blank" href="http://vk.com/cabinetio">
                            <?=t('VK') ?>
                        </a>
                        <?php endif ?>
                        <a target="_blank" href="<?=t('http://www.facebook.com/428565110591304')?>">
                            <?=t('Facebook') ?>
                        </a>
                        <a target="_blank" href="<?=t('http://twitter.com/cabinetio')?>">
                            <?=t('Twitter') ?>
                        </a>
                        <?php if(Yii::app()->language == 'ru'): ?>
                        <a target="_blank" href="https://plus.google.com/b/100347856296287562542/100347856296287562542/posts">
                            <?=t('Google+') ?>
                        </a>
                        <?php endif ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">

                <div class="widgets">
                </div>

                <div class="span12">
                    <div class="copy">
                        <p><?=t('Copyright') ?> © <?php
                            $start = 2013;
                            $date = ($start < date('Y') ? $start . '-' . date('Y') : $start); echo $date;
                            ?> <?=t('Cabinet') ?></p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </footer>

    <!-- JS -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/jquery.isotope.js"></script>
    <!-- Isotope for gallery -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/jquery.prettyPhoto.js"></script>
    <!-- prettyPhoto for images -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/jquery.cslider.js"></script>
    <!-- Parallax slider -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/modernizr.custom.28468.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/filter.js"></script>
    <!-- Filter for support page -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/cycle.js"></script>
    <!-- Cycle slider -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/jquery.flexslider-min.js"></script>
    <!-- Flex slider -->

    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/easing.js"></script>
    <!-- Easing -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/custom.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-8615631-24']);
        _gaq.push(['_setDomainName', 'cabinet.io']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

</body>
</html>