alter table User website is null
alter table User photo is null
alter table User email is null
alter table User secondname  is null


CREATE TABLE IF NOT EXISTS `remix` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contest_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;


ALTER TABLE  `users` ADD  `city` VARCHAR( 100 ) NULL ,
ADD  `country` VARCHAR( 100 ) NULL


ALTER TABLE  `users` ADD  `vk_id` BIGINT( 20 ) NULL COMMENT  'vkontakte',
ADD  `fb_id` BIGINT( 20 ) NULL COMMENT  'facebook',
ADD  `sc_id` BIGINT( 20 ) NULL COMMENT  'suncloud'




DROP TABLE IF EXISTS `meta`;
CREATE TABLE IF NOT EXISTS `meta` (
  `id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `slogan` varchar(250) CHARACTER SET utf8 NOT NULL,
  `logo` varchar(250) CHARACTER SET ucs2 NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `keywords` text CHARACTER SET utf8 NOT NULL,
  `vk` varchar(250) CHARACTER SET utf8 NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(250) CHARACTER SET utf8 NOT NULL,
  `google` varchar(250) CHARACTER SET utf8 NOT NULL,
  `youtube` varchar(250) CHARACTER SET utf8 NOT NULL,
  `contact1` text CHARACTER SET utf8 NOT NULL,
  `contact2` text CHARACTER SET utf8 NOT NULL,
  `contact3` text CHARACTER SET utf8 NOT NULL,
  `info` text CHARACTER SET utf8 NOT NULL,
  `facebook_client_id` varchar(100) NOT NULL,
  `facebook_client_secret` varchar(100) NOT NULL,
  `vkontakte_client_id` varchar(100) NOT NULL,
  `vkontakte_client_secret` varchar(100) NOT NULL,
  `soundcloud_client_id` varchar(100) NOT NULL,
  `soundcloud_client_secret` varchar(100) NOT NULL,
  `soundcloud_return_url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `meta`
--

INSERT INTO `meta` (`id`, `name`, `slogan`, `logo`, `title`, `description`, `keywords`, `vk`, `twitter`, `facebook`, `google`, `youtube`, `contact1`, `contact2`, `contact3`, `info`, `facebook_client_id`, `facebook_client_secret`, `vkontakte_client_id`, `vkontakte_client_secret`, `soundcloud_client_id`, `soundcloud_client_secret`, `soundcloud_return_url`) VALUES
('SendRemix', 'Send Remix', 'сервис музыкальных конкурсов', '', 'Send Remix', '', '', '', '', '', '', '', '<h4>Свяжись с нами</h4>\r\n							<p>+31 (0) 624 707 46</p>\r\n							<p>ADE@sendingmedia.com</p>', '<h4>Украина</h4>\r\n							<p>бул. Шевченко, 54/1, 7 этаж 01032 Киев</p>\r\n							<p>sendingmedia.com</p>\r\n							<p>brg.ua</p>', '<h4>Нидерланды</h4>\r\n							<p>Oudezijds Kolk 83</p>\r\n							<p>1012 AL Amsterdam</p>', 'информация про сервис', '771415536207883', '4cd8a61708b3c4b01619894c03ca23c5', '4007300', 'Se1fGd7xVABC02wjMpwV', '0d9e8790d25367f389940e4007600eb9', '54fd437db859f6dfac01b17e56d5520d', 'http://sendremix.futurity.pro/accounts/soundcloud');


ALTER TABLE  `meta` ADD  `title_prizes` VARCHAR( 100 ) NULL AFTER  `title` ,
ADD  `title_requirements` VARCHAR( 100 ) NULL AFTER  `title_prizes` ,
ADD  `title_about` VARCHAR( 100 ) NULL AFTER  `title_requirements` ;


ALTER TABLE  `meta` CHANGE  `title_prizes`  `title_prizes` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
ALTER TABLE  `meta` CHANGE  `title_requirements`  `title_requirements` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
ALTER TABLE  `meta` CHANGE  `title_about`  `title_about` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;


UPDATE  `sendremix`.`meta` SET  `title_prizes` =  'Призы' WHERE  `meta`.`id` =  'SendRemix';
UPDATE  `sendremix`.`meta` SET  `title_requirements` =  'Условия участия' WHERE  `meta`.`id` =  'SendRemix';
UPDATE  `sendremix`.`meta` SET  `title_about` =  'о Лейбле' WHERE  `meta`.`id` =  'SendRemix';


ALTER TABLE  `contests` CHANGE  `downloads`  `downloads` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL
ALTER TABLE  `users` ADD  `role` SMALLINT( 1 ) UNSIGNED NOT NULL DEFAULT  '1';
UPDATE  `sendrimix`.`users` SET  `role` =  '2' WHERE  `users`.`id` = 4;

ALTER TABLE  `remix` ADD INDEX (  `user_id` ) ;

ALTER TABLE  `remix` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `sendrimix`.`users` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;


ALTER TABLE  `remix` ADD  `original_file_name` VARCHAR( 100 ) NOT NULL ;