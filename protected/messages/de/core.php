<?php

return array(
    // site/index
    'en' => 'de',
    'en_US' => 'de_DE',
    'http://www.facebook.com/428565110591304' => 'http://www.facebook.com/434059270043272',
    'Cabinet' => 'Kabinett',
    'Personal finance management' => 'Verwalten Sie Ihre persönlichen Finanzen',
    'Home' => 'Zuhause',
    'About' => 'Über uns',
    'Cabinet. New User' => 'Kabinett. Neuer Benutzer',
    'Login' => 'Anmelden',
    'or' => 'oder',
    'Registration' => 'Registrierung',
    'Password' => 'Passwort',
    'Forgot Password?' => 'Passwort vergessen?',
    'Password Recovery' => 'Passwort Rückgewinnung',
    'Recover Password' => 'Reestablish Passwort',
    'Please, enter your email' => 'Bitte geben Sie Ihre E-Mail',
    'Please, login' => 'Melden Sie sich bitte',
    'Fill-in the fields, please' => 'Fill-in den Feldern, bitte',
    'Follow us' => 'Folgen Sie uns',
    'VK' => 'VK',
    'Facebook' => 'Facebook',
    'Google+' => 'Google+',

    // /cabinet/accounts/profile
    'Profile' => 'Profil',
    'Logout' => 'Ausgabe',
    'Resources' => 'Ressourcen',
    'Finance' => 'Finanzen',
    'Language {language} not found' => 'Язык {language} не найден',
    'First Name' => 'Vorname',
    'Family Name' => 'Familienname',
    'Phone' => 'Telefon',
    'Repeat Password' => 'Passwort wiederholen',
    'Join' => 'Registriert',

    // finance/index
    'jan' => 'jan',
    'feb' => 'feb',
    'mar' => 'mär',
    'apr' => 'apr',
    'may' => 'mai',
    'jun' => 'jun',
    'jul' => 'jul',
    'aug' => 'aug',
    'sep' => 'sep',
    'oct' => 'okt',
    'nov' => 'nov',
    'dec' => 'dez',

    'Date' => 'Datum',
    'Amount' => 'Summe',
    'Description' => 'Beschreibung',
    'Agent' => 'Agenten',
    'Category' => 'Kategorie',
    'Account' => 'Konto',

    'Add transaction' => 'Fügen Sie eine Transaktion',

    'Apply' => 'Anwenden',
    'Calendar' => 'Um planen',
    'Remove' => 'Entfernen',
    'Actualize' => 'Überprüfen Konto',
    'Total' => 'Summe',
    'Budget' => 'Budget',
    'Fact' => 'Fakt',
    'Plan' => 'Plan',

    //
    'Save' => 'Speichern',
    'Registration Confirmation' => 'Anmeldebestätigung',

    // finance hints
    'New transaction' => 'New transaction',
    'Start typing the amount to add a new transaction' => 'Start typing the amount to add a new transaction',
    'Editing transaction' => 'Editing transaction',
    'Press Enter to move to the next field. You can also switch between the fields using keys to left/right. The date of the transaction can be selected in the top left corner of the page' => 'Press Enter to move to the next field. You can also switch between the fields using keys to left/right. The date of the transaction can be selected in the top left corner of the page',
    'Account and Budget' => 'Account and Budget',
    'Ctrl + up/down - the account. Alt + up/down - the budget. You can also select the account in the table by clicking on the appropriate line on accounts table' => 'Ctrl + up/down - the account. Alt + up/down - the budget. You can also select the account in the table by clicking on the appropriate line on accounts table',
    'Saving transaction' => 'Saving transaction',
    'Click in this area to preserve the current transaction' => 'Click in this area to preserve the current transaction',
);
