<p>Добро пожаловать в Кабинет, <?php echo $user->first_name ?>.</p>
<p>Для начала работы <a href="<?php echo Yii::app()->params['host_name'] ?>/accounts/confirm?hash=<?php echo $user->hash ?>">подтвердите ваш email</a></p>

<br>
--<br>
С уважением,<br>
Алексей Разбаков<br>
ceo@cabinet.io<br>
<br>
Будут вопросы или идеи &mdash; обращайтесь.