<?php
/* @var $this RemixController */
/* @var $model Remix */

$this->breadcrumbs=array(
	'Remixes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Remix', 'url'=>array('index')),
	array('label'=>'Create Remix', 'url'=>array('create')),
	array('label'=>'View Remix', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Remix', 'url'=>array('admin')),
);
?>

<h1>Update Remix <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>