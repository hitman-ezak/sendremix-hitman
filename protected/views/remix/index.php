<?php
/* @var $this RemixController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Remixes',
);

$this->menu=array(
	array('label'=>'Create Remix', 'url'=>array('create')),
	array('label'=>'Manage Remix', 'url'=>array('admin')),
);
?>

<h1>Remixes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
