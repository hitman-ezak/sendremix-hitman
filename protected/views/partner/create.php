<?php
$this->breadcrumbs = array(
    'PR' => array('admin'),
	t('Partners') => array('admin'),
	t('Create'),
);

?>

<h1><?=t('Create Partner')?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>