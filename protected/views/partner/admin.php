<?php
$this->breadcrumbs=array(
    t('PR') => array('admin'),
	t('Partners'),
);

?>

<h1><?=t('Manage Partners') ?></h1>

<?php echo CHtml::link(t('Create Partner'), array('/partner/create'), array('class'=>'btn btn-primary')); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'partner-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'link',
		'image',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
