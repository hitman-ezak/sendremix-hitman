<?php
$this->breadcrumbs=array(
    t('Content')=>array('/site/meta'),
	t('Contests'),
);
?>
<h1><?=t('Manage members')?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'members-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns'=>array(

		'contest'=>array(
			'name'=>'contest',
			'header'=> t('Contest'),
			'value'=> '$data->contest()->name'
		),

		'author'=>array(
			'name'=>'author',
			'header'=> t('User'),
			'value'=> '$data->user()->first_name . \' \'. $data->user()->last_name'
		),

		'file_name'=>array(
			'name' =>'file_name',
			'header'=> t('File Name'),
			'type' => 'Raw',
			'value'=> 'CHtml::link(CHtml::encode($data->file_name), Remix::get_path($data->file_name))'
		),

		'original_file_name'=>array(
			'name' =>'original_file_name',
			'header'=> 'Оригинальное ' . t('File Name'),
			'value'=> '$data->original_file_name'
		),


		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view}',
			'buttons' => array(
				'view' => array(
					'url' => 'Yii::app()->createUrl("contest/memberView", array("id"=>$data->id))',
				),
			),
		),
	),
)); ?>
