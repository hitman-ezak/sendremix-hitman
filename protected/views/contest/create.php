<?php
$this->breadcrumbs=array(
    t('Content')=>array('/site/meta'),
	t('Contests')=>array('admin'),
	t('Create'),
);
?>

<h1><?=t('Create Contest')?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>