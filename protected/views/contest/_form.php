<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'contest-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php if( !$model->isNewRecord) {?>
    <div class="row">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons'=>array(
                array(
                    'label' => t('Change slide').' 1020 x 500',
                    'icon'=>'picture',
                    'htmlOptions'=>array(
                        'data-toggle'=>'modal',
                        'data-target'=>'#uploadSlide',
                    ),
                ),
                array('items'=>array(
                    array('label' => t('Remove slide'), 'url'=>array('/contest/removeSlide?id='.$model->id)),
                )),
            ),
        )); ?>
        <br><br>
    </div>

    <div class="row contest-slide">
        <?php if($model->slide): ?>
            <img id="slide" src="<?php echo $model->slide ?>">
        <?php else: ?>
            <img id="slide" src="/images/slide.png">
        <?php endif;?>
    </div>
    <br><br>

    <div class="row">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons'=>array(
                array(
                    'label' => t('Change cover').' 220 x 288',
                    'icon'=>'picture',
                    'htmlOptions'=>array(
                        'data-toggle'=>'modal',
                        'data-target'=>'#uploadCover',
                    ),
                ),
                array('items'=>array(
                    array('label' => t('Remove cover'), 'url'=>array('/contest/removeCover?id='.$model->id)),
                )),
            ),
        )); ?>
        <br><br>
    </div>

    <div class="row contest-cover">
        <?php if($model->cover): ?>
            <img id="cover" src="<?php echo $model->cover ?>">
        <?php else: ?>
            <img id="cover" src="/images/cover.png">
        <?php endif;?>
    </div>
    <br><br>
	<?} else {?>
		<p>*Загрузка слайда и обложки конкурса доступно после его создания</p>
	<?php }?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'author',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <?php echo $form->textAreaRow($model,'description_under',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'date_from',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'date_to',array('class'=>'span5')); ?>

    <?php echo $form->textAreaRow($model,'video',array('rows'=>6, 'cols'=>50, 'class'=>'span8 mceNoEditor')); ?>

    <?php echo $form->textAreaRow($model,'audio',array('rows'=>6, 'cols'=>50, 'class'=>'span8 mceNoEditor')); ?>

    <?php echo $form->textFieldRow($model, 'downloads',array('class'=>'span5')); ?>

    <!--div class="row">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons'=>array(
                array(
                    'label' => t('Add downloads').' (zip)',
                    'icon'=>'picture',
                    'htmlOptions'=>array(
                        'data-toggle'=>'modal',
                        'data-target'=>'#uploadDownloads',
                    ),
                ),
                array('items'=>array(
                    array('label' => t('Remove downloads'), 'url'=>array('/contest/removeDownloads?id='.$model->id)),
                )),
            ),
        )); ?>
        <br><br>
    </div-->

    <!--div class="row contest-cover">
        <?php if($model->downloads): ?>
            <div id="downloads"><?php echo $model->downloads ?></div>
        <?php else: ?>
            <div id="downloads"><?php echo t('No file') ?></div>
        <?php endif;?>
    </div-->
    <br><br>

	<?php echo $form->textAreaRow($model,'more_requirements',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'more_label',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'prize',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'more_prize',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <?php echo $form->textFieldRow($model,'web',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'vk',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'facebook',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'twitter',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? t('Create') : t('Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'uploadCover')); ?>

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4><?=t('Upload Cover')?></h4>
    </div>

    <div class="modal-body">
        <?php
        $this->widget('ext.EAjaxUpload.EAjaxUpload',
            array(
                'id' => 'uploadFile',
                'config' => array(
                    'action' => Yii::app()->createUrl('/contest/uploadCover'),
                    'allowedExtensions' => array("jpg", "jpeg", "gif", "png"),
                    'sizeLimit' => 10*1024*1024,
                    'minSizeLimit' => 1,
                    'onComplete'=>"js:function(id, fileName, responseJSON){
					$('#cover').attr('src', '/uploads/'+responseJSON.filename);
					$.post('".Yii::app()->createUrl('/contest/changeCover')."?id=".$model->id."', {filename: '/uploads/' + responseJSON.filename});
				}"
                )
            ));
        ?>
    </div>

    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => t('Close'),
            'url' => '#',
            'htmlOptions' => array('data-dismiss'=>'modal'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'uploadSlide')); ?>

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4><?=t('Upload Slide')?></h4>
    </div>

    <div class="modal-body">
        <?php
        $this->widget('ext.EAjaxUpload.EAjaxUpload',
            array(
                'id' => 'uploadFileSlide',
                'config' => array(
                    'action' => Yii::app()->createUrl('/contest/uploadSlide'),
                    'allowedExtensions' => array("jpg", "jpeg", "gif", "png"),
                    'sizeLimit' => 10*1024*1024,
                    'minSizeLimit' => 1,
                    'onComplete'=>"js:function(id, fileName, responseJSON){
					$('#slide').attr('src', '/uploads/'+responseJSON.filename);
					$.post('".Yii::app()->createUrl('/contest/changeSlide')."?id=".$model->id."', {filename: '/uploads/' + responseJSON.filename});
				}"
                )
            ));
        ?>
    </div>

    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => t('Close'),
            'url' => '#',
            'htmlOptions' => array('data-dismiss'=>'modal'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>


<!--?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'uploadDownloads')); ?>

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4><?=t('Upload Downloads')?></h4>
    </div>

    <div class="modal-body">
        <?php
        $this->widget('ext.EAjaxUpload.EAjaxUpload',
            array(
                'id' => 'uploadFileDownloads',
                'config' => array(
                    'action' => Yii::app()->createUrl('/contest/uploadDownloads'),
                    'allowedExtensions' => array("zip"),
                    'sizeLimit' => 10*1024*1024,
                    'minSizeLimit' => 1,
                    'onComplete'=>"js:function(id, fileName, responseJSON){
					$('#downloads').html(responseJSON.filename);
					$.post('".Yii::app()->createUrl('/contest/changeDownloads')."?id=".$model->id."', {filename: '/uploads/' + responseJSON.filename});
				}"
                )
            ));
        ?>
    </div>

    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => t('Close'),
            'url' => '#',
            'htmlOptions' => array('data-dismiss'=>'modal'),
        )); ?>
    </div -->

<?//php $this->endWidget(); ?>