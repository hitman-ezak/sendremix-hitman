<?php
$this->breadcrumbs=array(
    t('Content')=>array('/site/meta'),
	t('Members'),
);


?>
<?php echo CHtml::link('Вернуться назад', array('/contest/members')) ?>

<br/>
<br/>

<div class="view">
	<?php $user = $model->user();?>
	<?php $this->widget('bootstrap.widgets.TbDetailView', array(
		'data'=> $model,
		'attributes'=>array(

			'contest' => array(
				'name' =>  t('Contest'),
				'value' => $model->contest()->name
			),
			'user_id' => array(
				'name' => t('Member'),
				'value' => $user->last_name    . ' ' .
						   $user->first_name   . ' ' .
						   $user->second_name
			),

			'file_name' => array(
				'type'   =>'raw',
				'name'  => t('Remix'),
				'value' => CHtml::link($model->file_name, Remix::get_path($model->file_name))
			),


			'created_on' => array(
				'name' => t('Date Added'),
				'value' => $model->get_create_on()
			),

			'phone' => array(
				'name' => t('Phone'),
				'value' => $user->phone,
			),
			'country' => array(
				'name' => t('Country'),
				'value' => $user->country,
			),
			'city' => array(
				'name' => t('City'),
				'value' => $user->city,
			),

			'email' => array(
				'name' => 'email',
				'value' => $user->email,
			),
			'website' => array(
				'name' => t('Website'),
				'value' => $user->website,
			),


			'photo' => array(
				'type'=>'html',
				'name' => 'Фото',
				'value' => '<img width="300px" src="' .$user->getPhoto() .'"/>',
			),





		),
	)); ?>

</div>

