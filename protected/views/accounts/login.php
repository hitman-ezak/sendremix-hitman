<div class='content-wrapper sign-in-page' style="margin: 0 auto; width: 400px">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>

	<div class="form-header">
		<h3><?php echo t('Login')?></h3>
	</div>

	<div class="form-content">
		<div class="one-column-wrapper">
			<div class="info-field">
				<div class="info-label">
					<div class="left">Email: </div>
					<!--div class="right"><?php echo CHtml::link('Register', array('member/register')); ?></div-->
				</div>
				<?php echo $form->textField($model,'username'); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
		</div>

		<div class="one-column-wrapper">
			<div class="info-field">
				<div class="info-label">
					<div class="left">Password: </div>
					<!--div class="right"><?php echo CHtml::link('Forgot password?', array('/forgot-password')); ?></div-->
				</div>

				<?php echo $form->passwordField($model,'password'); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
		</div>
	</div>

	<div class="form-buttons-wrapper">
		<?php echo CHtml::submitButton(t('Sign In'), array('class'=>'blue-square__button')); ?>
	</div>

	<?php $this->endWidget(); ?>

</div>
