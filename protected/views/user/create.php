<?php
$this->breadcrumbs = array(
	t('Users')  => array('admin'),
	t('Create'),
);
?>

<h1><?php echo t('Create User')?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>