<h1><?php echo t('Manage Users')?></h1>
<?php echo CHtml::link(t('Create User'), array('user/create'), array('class'=>'btn btn-primary')); ?>

<?php
$this->breadcrumbs=array(
	t('Users'),
);
?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'email',
		'phone',

		'name' => array(
			'name' => 'first_name',
			'value' => '$data->first_name . " " . $data->last_name'
		),
		/*
		'last_name',
		'second_name',
		'website',
		'photo',
		'hash',
		'confirmed',
		'language',
		'city',
		'country',
		'vk_id',
		'fb_id',
		'sc_id',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
)); ?>
