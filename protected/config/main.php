<?php
function lower($str) {
    return mb_strtolower($str, 'utf-8');
}

function t($message, $params = array()) {
    return Yii::t('core', $message, $params);
}

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('cabinet-widgets', dirname(__FILE__).'/../extensions/cabinet');


return array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'homeUrl'=>array('contest/index'),
	'defaultController' => 'contest/index',
	'name' => 'SendRemix',
		
	'theme'=>'metro',

    'sourceLanguage' => 'en',
    'language' => 'ru',
		
	//'preload' => array('log'),

	'import' => array(
		'application.models.*',
		'application.components.*',
        'application.components.yii-mail.*',
		'ext.eoauth.*',
		'ext.eoauth.lib.*',
		'ext.lightopenid.*',
		'ext.eauth.services.*',
	),

	'modules' => array(
		'gii'=>array(
			'class' => 'system.gii.GiiModule',
			'password' => 'admin',
			//'ipFilters' => array('127.0.0.1','::1'),
			'generatorPaths' => array(
				'bootstrap.gii',
			),
		),

        'yiicCommandMap' => array(
            'migrate' => array(
                'class' => 'system.cli.commands.MigrateCommand',
                'migrationPath' => 'application.migrations',
                'migrationTable' => 'migrations',
                'connectionID' => 'db',
                'templateFile' => 'application.migrations.template',
            ),
        ),
	),

	// application components
	'components' => array(
		'loid' => array(
			'class' => 'ext.lightopenid.loid',
		),
		'authManager' => array(
			'class' 	   => 'PhpAuthManager',
			'defaultRoles' => array('guest'),
		),

		'clientScript'=>array(
			'packages' => require dirname(__FILE__) . DIRECTORY_SEPARATOR .  'packages.php',
		),

		'eauth' => array(
			'class' => 'ext.eauth.EAuth',
			'popup' => true, // Use the popup window instead of redirecting.
			'services' => array( // You can change the providers and their classes.
				'facebook' => array(
					'class'			=> 'FacebookOAuthService',
					'client_id' 	=> 'see site/meta',
					'client_secret' =>  'see site/meta',
				),
				'vkontakte' => array(
					'class'			 => 'VKontakteOAuthService',
					'client_id'		 => 'see site/meta',
					'client_secret'  => 'see site/meta',
				),
				'soundcloud' => array(
					'class'			 => 'SoundCloudOAuthService',
					'client_id'		 => 'see site/meta',
					'client_secret'  => 'see site/meta',
				),

			),
		),
		'user' => array(
            'class' => 'WebUser',
			'allowAutoLogin' => true,
            'autoUpdateFlash' => false,
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.Bootstrap',
		),

        'errorHandler' => array(
            'errorAction' => '/site/error',
        ),

		'urlManager' => array(
            //'class' => 'application.extensions.urlManager.LangUrlManager',
            //'languages' => array('ru', 'en'),
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '' => 'contest/index',
                'admin' => 'accounts/login',
				'logout' => 'accounts/logout',
				//'<url>' =>array('site/page'),
                //'<lang:\\w+>' => 'contest/index',
                //'<lang:\\w+>/contest' => 'contest',
                // '<lang:\\w+>/accounts/confirm/<hash>' => 'accounts/confirm',
                //  '<lang:\\w+>/gii' => 'gii',
                // '<lang:\\w+>/<url>' => 'site/page',
                //  '<lang:\\w+>/<controller:\\w+>/<action:\\w+>' => '<controller>/<action>',
               // '<lang:\\w+>/<controller:\\w+>' => '<controller>',
            ),
		),

        'db' => require(dirname(__FILE__).'/database.php'),

		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),

				array(
					'class'=>'CWebLogRoute',
				),

			),
		),
        'mail' => array(
            'class' => 'application.components.yii-mail.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'adminEmail' =>'aleksey@razbakov.com',
		'host_name' => 'http://sendremix.futurity.pro',

		// AdminPanel access
		'admins' => array('admin@mail.ru', 'info@sendremix.futurity.pro'),


		//gmail/facebook/soundcloud accounts: sendrimix@gmail.com/k0crHEGFzSgFEmUd7Pl9
	),
);