<?php
return array(
	User::ROlE_GUEST => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Guest',
		'bizRule' 	  => null,
		'data' 		  => null
	),
	 User::ROLE_USER => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' 	=> 'User',
		'children' 		=> array(
			'guest',
		),
		'bizRule' => null,
		'data' => null
	),

	User::ROlE_ADMIN => array(
		'type' 			=> CAuthItem::TYPE_ROLE,
		'description'   => 'Administrator',
		'children' 		=> array(
			'user',
		),
		'bizRule'		 => null,
		'data' 			 => null
	),
);