<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/main';

    /**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();

    public $contact1;
    public $contact2;
    public $contact3;

    public $twitter;
    public $facebook;
    public $vk;
    public $youtube;


    public $partners;

    public $name;
    public $slogan;
    public $title;
    public $description;
    public $keywords;

	public $user;
	public $model;
	public $meta;

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

    public function init() {
        $meta = Meta::model()->find('id="'.Yii::app()->name.'"');
        $partners = Partner::model()->findAll();

        $this->contact1 = $meta->contact1;
        $this->contact2 = $meta->contact2;
        $this->contact3 = $meta->contact3;
        $this->twitter = $meta->twitter;
        $this->facebook = $meta->facebook;
        $this->partners = $partners;

        $this->name = $meta->name;
        $this->slogan = $meta->slogan;
        $this->title = $meta->title;
        $this->description = $meta->description;
        $this->keywords = $meta->keywords;

        if (!Yii::app()->user->isGuest) {
            Yii::app()->setTheme('ace');
        } else {
            Yii::app()->setTheme('sendremix');
        }

        //Yii::app()->urlManager->setAppLanguage();
    }
}