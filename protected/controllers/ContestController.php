<?php

class ContestController extends Controller
{
	public $layout='//layouts/main';

	public function filters() {
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array(
				'allow',
				'actions' => array(
					'UploadCover','ChangeCover' , 'RemoveCover', 'UploadSlide', 'ChangeSlide',
					'RemoveSlide', 'UploadRemix', 'CreateRemix'
				),
				'users'   => array('@')
			),
			array('allow',
				'actions' =>array('create','update','delete', 'admin' ,'members', 'MemberView'),
				'roles'   =>array(User::ROlE_ADMIN),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id)
	{
        Yii::app()->setTheme('sendremix');
		$this->SetUserModels();
		$meta = Meta::model()->find('id="'.Yii::app()->name.'"');
		$this->meta = $meta;
		$meta = Meta::getMetaModel();

		if ($meta) {
			$this->render('view',array(
				'model'		=> $this->loadModel($id),
				'modal_id'  => $this->getModalId(),
				'meta'      => $meta,
			));
		} else {
			throw new CHttpException(404,'Can\'t find site meta information. Please contact with administrator');
		}
	}

	public function actionCreate()
	{

		$model = new Contest;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Contest']))
		{
			$model->attributes = $_POST['Contest'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create', array(
			'model'=>$model,
		));
	}


	public function actionUpdate($id)
	{

		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Contest']))
		{
			$model->attributes = $_POST['Contest'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/*
	 * Upload remix by authorized user
	 * Remix file name is storing by session
    */
	public function actionCreateRemix()
	{
		$model = new User(User::SEND_REMIX);

		if(!empty(Yii::app()->session['remix'])){
			$model->file = Yii::app()->session['remix'];
		} else {

			$model->addError('file', 'Выберите файл ремикса');
		}
		unset(Yii::app()->session['remix']);
		$this->performAjaxValidationRemix($model);

		if(isset($_POST['User']))
		{
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$user_id =  Yii::app()->user->id;
				$user = User::model()->findByPk($user_id);
				$user->attributes = $_POST['User'];

				if($user->save()){

					$parser = new CHtmlPurifier();
					$file = $parser->purify($_POST['User']['file']);

					$contest_id =  (int) $_POST['User']['contest_id'];
					$contest = Contest::model()->findByPk($contest_id);

					if (!empty($contest)) {

						$remix = new Remix();
						$remix->contest_id = $contest_id;
						$remix->user_id = $user->id;
						$remix->file_name = $file;
						$remix->original_file_name = Yii::app()->session['original_file_name'];

						$remix->save();
						unset(Yii::app()->session['original_file_name']);
						$transaction->commit();

					} else {
						$user->addError('contest_id', 'Wrong contest information');
						$transaction->rollback();
					}

				}
			} catch (Exception $e) {
				$transaction->rollback();
				echo $e; die();
			}
		}
		$this->redirect('/');
	}

	public function actionUploadRemix()
	{
		Yii::import("ext.EAjaxUpload.qqFileUploader");
		$folder = dirname(Yii::getPathOfAlias('application')).'/uploads/remixes/';
		$allowedExtensions = array('mp3');
		$sizeLimit = 20 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder, true);
		Yii::app()->session['remix'] = $result['filename'];
		Yii::app()->session['original_file_name'] = $result['original_file_name'];
		$result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		echo $result;
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex()
	{
        Yii::app()->setTheme('sendremix');
        $models = Contest::model()->findAll();
		$partners = Partner::model()->findAll();
        $meta = Meta::model()->find('id="'.Yii::app()->name.'"');
		$this->meta = $meta;
		$modal_id =  $this->GetModalId();
		$this->SetUserModels();
		$this->render('index', compact('models', 'meta', 'modal_id', 'partners'));
	}

	public function actionAdmin()
	{
		$downloads = DowloadsStat::model()->findAll();
		$remixes   = Remix::model()->findAll();

		$download_stat = 0;
		$remixes_stat = 0;

		if (!empty($downloads)) {
			$download_stat = count($downloads);
		}

		if (!empty($remixes)) {
			$remixes_stat =  count($remixes);
		}

		$model = new Contest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contest']))
			$model->attributes = $_GET['Contest'];

		$this->render('admin', array(
			'model' 		=> $model,
			'download_stat' => $download_stat,
			'remixes_stat'  => $remixes_stat,
		));
	}

	public function actionMembers()
	{

		$model = new Remix('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contest']))
			$model->attributes = $_GET['Contest'];

		$this->render('members', array(
			'model' => $model,
		));
	}

	public function actionMemberView($id)
	{

		$model = Remix::model()->with('user','contest')->findByPk($id);
		if (!empty($model)) {


			$this->render( 'member_view', array (
				'model' => $model,
			));
		} else {
			throw new CHttpException(404,'The requested page does not exist.');
		}
	}


    public function actionUploadCover()
    {
        Yii::import("ext.EAjaxUpload.qqFileUploader");
        $folder = dirname(Yii::getPathOfAlias('application')).'/uploads/';
        $allowedExtensions = array("jpg");
        $sizeLimit = 10 * 1024 * 1024;
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder, true);

        $result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $result;
    }


    public function actionChangeCover()
    {
        $filename = $_POST['filename'];
        $contest = Contest::model()->findByPk($_GET['id']);
        $contest->cover = $filename;
        $contest->save(false);
    }

    public function actionRemoveCover()
    {
        $contest = User::model()->findByPk($_GET['id']);
        $contest->cover = '';
        $contest->save(false);
        $contest->redirect(array('admin'));
    }

    public function actionUploadSlide()
    {
        Yii::import("ext.EAjaxUpload.qqFileUploader");
        $folder = dirname(Yii::getPathOfAlias('application')).'/uploads/';
        $allowedExtensions = array("jpg");
        $sizeLimit = 10 * 1024 * 1024;
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder, true);

        $result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $result;
    }

    public function actionChangeSlide()
    {
        $filename = $_POST['filename'];
        $contest = Contest::model()->findByPk($_GET['id']);
        $contest->slide = $filename;
        $contest->save(false);
    }

    public function actionRemoveSlide()
    {
        $contest = Contest::model()->findByPk($_GET['id']);
        $contest->slide = '';
        $contest->save(false);
        $contest->redirect(array('admin'));
    }


	/*	Subject to user authorized or not
	 *  this method create $user variable
	 *  with right validation scenario
	 * */
	private function setUserModels()
	{
		$is_guest = Yii::app()->user->isGuest;
		if($is_guest) {

			$this->model = new LoginForm();
			$this->user = new User(User::SIGNUP);
		} else {
			$this->user = new User(User::SEND_REMIX);
			$user_id = Yii::app()->user->id;
			$user  = User::model()->findByPk($user_id);
			$this->user = $user;
		}

	}

	/*
		if user doesn't login and click to upload remix,
		his must register or auth before and we need to show
		him right modal window
	*/
	private function getModalId()
	{
		$is_guest = Yii::app()->user->isGuest;
		if($is_guest) {
			return  '#modal_01';
		} else {
			return	'#modal-send-remix';
		}
	}

	public function loadModel($id)
	{
		$model = Contest::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{

		if(isset($_POST['ajax']) && $_POST['ajax'] === 'contest-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function performAjaxValidationRemix($model)
	{

		if(isset($_POST['ajax']) && $_POST['ajax'] === 'remix-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
