<?php

class AccountsController extends Controller
{

	public function filters() {
		return array(
			//'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',	'users'=>array('*')),
			array(
				'allow',
				'users'   => array('?'),
				'actions' => array('login', 'logout'),
			),

			array(
				'deny',
				'actions' => array(
					'Profile','Upload' ,'ChangePhoto',
				),
				'users'   => array('?')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionRegister()
	{
        $this->breadcrumbs = array(t('Registration'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Registration'));

		$user  = new User(User::SIGNUP);
		if(isset($_POST['User'])) {
			$user->attributes = $_POST['User'];
		}

		if(isset($_POST['ajax']) && $_POST['ajax'] === 'register-form')
		{
			echo CActiveForm::validate($user);
			Yii::app()->end();
		}
//            $user->first_name = CHtml::encode($user->first_name);
//            $user->last_name = CHtml::encode($user->last_name);
//            $user->email = CHtml::encode($user->email);
//            $user->phone = CHtml::encode($user->phone);
            $user->language = Yii::app()->language;
            if ($user->save()) {
                $user->hash = md5(time());
                $user->save(false);
                $message = new YiiMailMessage;
                $message->view = 'confirmation_' . Yii::app()->language;
                $message->setSubject('['.t(Yii::app()->name).'] '.t('Registration Confirmation'));
                $message->setBody(array('user'=>$user), 'text/html');
                $message->addTo($user->email);
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
                $this->redirect(array('accounts/welcome'));
            }
//        } else {
//            $this->render('register', array('form' => $form));
//        }
	}
	public function actionWelcome()
	{
        $this->breadcrumbs = array(t('Welcome'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Welcome'));

		$this->render('welcome');
	}
	public function actionSoundCloud()
	{
		if (isset($_GET['error']))
		{
			$this->redirect(Yii::app()->homeUrl);
		}

		$meta = Meta::model()->findByPk(Yii::app()->name);

		$client = new Services_Soundcloud(
			$meta->soundcloud_client_id,
			$meta->soundcloud_client_secret,
			$meta->soundcloud_return_url
		);

		if(isset($_GET['code'])) {
			$code = $_GET['code'];
			$access_token = $client->accessToken($code);
			$client->setAccessToken($access_token['access_token']);
			$info = json_decode($client->get('me'), true);
			$service = 'soundcloud';
			$db_field = $this->getDbFieldByService($service) ;
			$social_id = $info['id'];


			$user = User::model()->find("$db_field = $social_id");

			$user_identity = new UserIdentity($user->email, 'no-password-needed');

			if (!empty($user)) {
				$user_identity->fakeAuthenticate(array($db_field => $social_id));
				Yii::app()->user->login($user_identity);
			} else {
				$user = new User();
				$this->fillUserInfoByService($user, $service, $info);
				$user->$db_field = $social_id;
				$user->save();

				$user_identity->fakeAuthenticate(array($db_field => $social_id));
				Yii::app()->user->login($user_identity);

			}
			$this->redirect(Yii::app()->homeUrl);
		}

		$this->redirect($client->getAuthorizeUrl() ) ;

	}
	public function actionLogin()
	{
		if(Yii::app()->user->checkAccess(User::ROlE_ADMIN)){
			$this->redirect(array('site/meta'));
		} else if (Yii::app()->user->checkAccess(User::ROLE_USER)){
			$this->redirect(array('/'));
		}


        $this->breadcrumbs = array(t('Login'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Login'), 'utf-8');
		$service = Yii::app()->request->getQuery('service');

		if (isset($service)) {

			$authIdentity = Yii::app()->eauth->getIdentity($service);

			//$authIdentity->redirectUrl = Yii::app()->user->returnUrl;
			$authIdentity->redirectUrl = Yii::app()->user->returnUrl;

			$authIdentity->cancelUrl = $this->createAbsoluteUrl('accounts/login');

			if ($authIdentity->authenticate()) {
				$identity = new ServiceUserIdentity($authIdentity);

				// success enter
				if ($identity->authenticate()) {

					$attributes = $authIdentity->attributes;

					$social_id = $attributes['id'];
					$db_field = $this->getDbFieldByService($service) ;
					$user = User::model()->find("$db_field = $social_id");

					$user_identity = new UserIdentity($user->email, 'no-password-needed');
					if (!empty($user)) {
						$user_identity->fakeAuthenticate(array($db_field => $social_id));
						Yii::app()->user->login($user_identity);
					} else {
						$user = new User();
						$this->fillUserInfoByService($user, $service, $attributes);
						$user->$db_field = $social_id;
						$user->save();

						$user_identity->fakeAuthenticate(array($db_field => $social_id));
						Yii::app()->user->login($user_identity);
					}
					//Yii::app()->user->login($identity);

					// redirect with close popup
					$this->redirect('/');
					//$authIdentity->redirect();
				}
				else {
					// close popup and redirect to CancelUrl
					$authIdentity->cancel();
				}
			}

		}


		$model = new LoginForm();
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if(isset($_POST['LoginForm'])) {

			$model->attributes = $_POST['LoginForm'];
			if($model->validate() && $model->login()){
				if(Yii::app()->user->checkAccess(User::ROlE_ADMIN))
					$this->redirect(array('site/meta'));
				else {
					$this->redirect(Yii::app()->homeUrl);
				}
			}
		}
		$this->render('login', array('model'=> new LoginForm()));

	}
    public function actionForgotPassword()
    {
        $this->breadcrumbs = array(t('Password Recovery'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Password Recovery'));

        $model = new User;
        $form = new CForm('application.forms.password', $model);
        if ($form->submitted('password')) {
            $user = User::model()->findByAttributes(array('email' => strtolower($_POST['User']['email'])));
            if($user) {
                $password = time();
                $user->password = User::hashPassword($password);
                $user->save(false);
                $message = new YiiMailMessage;
                $message->view = 'password_' . Yii::app()->language;
                $message->setSubject('['.t(Yii::app()->name).'] '.t('Password Recovery'));
                $message->setBody(array('user'=>$user, 'password'=>$password), 'text/html');
                $message->addTo($user->email);
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
                Yii::app()->user->setFlash('success', t("Check your mail"));
                $this->redirect('/');
            } else {
                Yii::app()->user->setFlash('error', t("Wrong email"));
            }
        }

        $this->render('forgotPassword', array('form' => $form));
    }
    public function actionConfirm($hash)
    {
		$user = User::model()->find("hash='$hash'");
        if($user) {
            $user->hash = '';
            $user->confirmed = 1;
            $user->save(false);
            Yii::app()->user->setFlash('success', t("Your email was successfully confirmed"));
            $identity = new UserIdentity($user->email, $user->password);
            $identity->authenticate();
            Yii::app()->user->login($identity);

            $message = new YiiMailMessage;
            $message->view = 'new_user_' . Yii::app()->language;
            $message->setSubject('['.t(Yii::app()->name).'] '.t('New User'));
            $message->setBody(array('user'=>$user), 'text/html');
            $message->addTo(Yii::app()->params['adminEmail']);
            $message->from = $user->email;
            Yii::app()->mail->send($message);

            $this->redirect('/');
        } else {
            throw new CHttpException(404, t('Confirmation code is wrong'));
        }
    }
	public function actionLogout()
	{
		Yii::app()->user->logout();
		unset(Yii::app()->session['eauth_profile']);
		$this->redirect('/');
	}
	public function actionProfile()
	{
        $this->breadcrumbs = array(t('Profile'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Profile'));

		$user = User::model()->findByPk(Yii::app()->user->id);

		if(isset($_POST['User'])) {
			$user->scenario = User::PROFILE;
			$user->attributes = $_POST['User'];
			if($user->validate()) {
				$user->save(false);
				$this->redirect(array('site/meta'));
			}
		}

		$this->render('profile', array('model' => $user));
	}
	public function actionUpload()
	{
		Yii::import("ext.EAjaxUpload.qqFileUploader");
		$folder = dirname(Yii::getPathOfAlias('application')).'/uploads/';
		$allowedExtensions = array("jpg");
		$sizeLimit = 10 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder, true);

		$result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		echo $result;
	}
	public function actionChangePhoto()
	{
		$filename = $_POST['filename'];
		$user = User::model()->findByPk(Yii::app()->user->id);
		$user->photo = $filename;
		$user->save(false);
	}
	public function actionRemovePhoto()
	{
		$user = User::model()->findByPk(Yii::app()->user->id);
		$user->photo = '';
		$user->save(false);
		$this->redirect(array('profile'));
	}
	public function actionChangePassword()
	{
		$user = User::model()->findByPk(Yii::app()->user->id);
		$user->scenario = User::PASSWORD;
		$user->password = $_POST['password'];
		$user->save(false);
	}
	public function actionShowPopup()
	{
		$method = $_GET['method'];
		if (!empty($method) ){
		$session = Yii::app()->session;
		switch($method) {
			case 'set':
				$session['showPopup'] = 1;
				break;
			case 'remove':
				unset($session['showPopup']);
				break;
			}
		}
		return;
	}


	private function fillUserInfoByService($user_model, $service, $attributes){
		switch ($service) {
			case 'vkontakte':
				if(!empty ($attributes['name'])){
					$name = explode(' ', $attributes['name']);
					if(!empty($name[0]) AND !empty($name[1])){
						$user_model->first_name = $name[1];
						$user_model->last_name = $name[0];
					}
				}
				if(!empty($attributes['url'])){
					$user_model->website = $attributes['url'];
				}
				if(!empty($attributes['country'])){
					$user_model->country = $attributes['country'];
				}
				if(!empty($attributes['city'])){
					$user_model->country = $attributes['city'];
				}
				return $user_model;
				break;

			case 'facebook':
				if(!empty ($attributes['first_name'])){
					$user_model->first_name =$attributes['first_name'];
				}
				if(!empty ($attributes['email'])){
					$user_model->email = $attributes['email'];
				}

				if(!empty ($attributes['last_name'])){
					$user_model->last_name = $attributes['last_name'];
				}

				if(!empty($attributes['url'])){
					$user_model->website = $attributes['url'];
				}
				return $user_model;
				break;

			case 'soundcloud':
				if(!empty ($attributes['username'])){
					$name = explode(' ', $attributes['username']);
					if(!empty($name[0]) AND !empty($name[1])){
						$user_model->first_name = $name[1];
						$user_model->last_name = $name[0];
					}
				}
				if(!empty($attributes['uri'])){
					$user_model->website = $attributes['uri'];
				}
				if(!empty($attributes['avatar_url'])){
					$user_model->photo = $attributes['avatar_url'];
				}
				if(!empty($attributes['country'])){
					$user_model->country = $attributes['country'];
				}
				if(!empty($attributes['city'])){
					$user_model->country = $attributes['city'];
				}
				return $user_model;
				break;
		}
		return false;
	}
	private function getDbFieldByService($service)
	{
		switch ($service) {
			case 'vkontakte':
				return 'vk_id';
				break;
			case 'soundcloud':
				return 'sc_id';
				break;
			case 'facebook':
				return 'fb_id';
				break;

		}
		return false;

	}
}