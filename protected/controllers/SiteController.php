<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function filters() {
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'roles'   => array(User::ROlE_ADMIN)
			),
			array(
				'allow',
				'users'   => array('?'),
				'actions' => array('about','contacts','logout','login'),
			),

			array(
				'allow',
				'users'   => array('?'),
				'actions' => array('page'),
			),


			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
        $this->breadcrumbs = array(t('Home'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Send Remix'));

		$this->render('index');
	}

    public function actionMeta()
    {
		$this->breadcrumbs = array(t('Content')=>array('/site/meta'), t('Meta'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Meta'));

        $meta = Meta::model()->findByPk(Yii::app()->name);


        if(isset($_POST['Meta'])) {

            $meta->attributes = $_POST['Meta'];
			if($meta->validate()) {

                $meta->save(false);

                $this->redirect( array('site/meta'));
            }
        }

        $this->render('meta', array('model' => $meta));
    }

    public function actionPage()
    {
        Yii::app()->setTheme('sendremix');

        if (isset($_GET['url'])) {
            $page = Page::model()->find('url="'.$_GET['url'].'"');
        } else {
            $page = false;
        }

        if ($page) {
            $this->breadcrumbs = array($page->title);
            $this->pageTitle = $page->title . ' — ' . t(Yii::app()->name);
            $this->render('page', array('model' => $page));
        } else {
            $this->render('404');
        }
    }

    public function actionPages()
    {

        $this->breadcrumbs = array(t('Content')=>array('/site/meta'), t('Pages'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('Pages'));


        $pages = Page::model()->findAll('site="'.Yii::app()->name.'"');
        if(isset($_GET['id']) && $_GET['id']>0) {
            $page = Page::model()->findByPk($_GET['id']);
        } elseif(isset($_GET['id']) && $_GET['id']==-1) {
            $page = new Page;
        } else {
            $page = false;
        }

        if(isset($_POST['Page']) && $page) {
            $page->attributes = $_POST['Page'];
            $page->site = Yii::app()->name;
            if($page->validate()) {
                $page->save(false);
                $this->redirect(array('/site/pages', 'id' => $page->id));
            }
        }

        $this->render('pages', array('pages' => $pages, 'model' => $page));
    }

    public function actionAbout()
    {
        $this->breadcrumbs = array(t('About'));
        $this->pageTitle = t(Yii::app()->name) . ' — ' . lower(t('About'));

        Yii::app()->setTheme('sendremix');

        $this->render('about');
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else {
                $this->render('error', $error);
            }
		}
	}

	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	public function actionUploadLogo()
	{
		Yii::import("ext.EAjaxUpload.qqFileUploader");
		$folder = dirname(Yii::getPathOfAlias('application')).'/uploads/';
		$allowedExtensions = array("jpg", "jpeg", "gif", "png");
		$sizeLimit = 10 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder, true);
		$result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		echo $result;
	}

	public function actionChangeLogo()
	{
		$filename = $_POST['filename'];
		$meta = Meta::model()->findByPk(Yii::app()->name);
		$meta->logo = $filename;
		$meta->save(false);
	}

	public function actionDownloadStat()
	{
		if(Yii::app()->request->isAjaxRequest AND !Yii::app()->user->isGuest) {
			$contest_id = (int) $_GET['contest'];
	    	$user_id = Yii::app()->user->id;
			$stat_exist = DowloadsStat::model()->findByAttributes(array('contest_id' => $contest_id, 'user_id' => $user_id));
			if (empty($stat_exist)){
				$stat = new DowloadsStat();
				$stat->user_id = $user_id;
				$stat->contest_id = $contest_id;
				$stat->save();
			}
		}
	}


}