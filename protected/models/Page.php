<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $site
 * @property string $url
 * @property string $created
 * @property string $modified
 * @property integer $author
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $body
 * @property string $published_from
 * @property string $published_to
 * @property integer $deleted
 * @property string $thumb
 */
class Page extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('site, url, title', 'required'),
			array('author, deleted', 'numerical', 'integerOnly'=>true),
			array('site', 'length', 'max'=>50),
			array('url, title, thumb', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, site, url, created, modified, author, title, description, keywords, body, published_from, published_to, deleted, thumb', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => t('Url'),
			'created' => t('Created'),
			'modified' => t('Modified'),
			'author' => t('Author'),
			'title' => t('Title'),
			'description' => t('Description'),
			'keywords' => t('Keywords'),
			'body' => t('Body'),
			'published_from' => t('Published From'),
			'published_to' => t('Published To'),
			'deleted' => t('Deleted'),
			'thumb' => t('Thumb'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('site',$this->site,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('author',$this->author);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('published_from',$this->published_from,true);
		$criteria->compare('published_to',$this->published_to,true);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('thumb',$this->thumb,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d H:i:s');
            }

            $this->modified = date('Y-m-d H:i:s');

            return true;
        }

        return false;
    }
}