<?php

class Remix extends CActiveRecord
{
	public  $author;
	public  $contest;
	private static  $upload_dir = '/uploads/remixes/';

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'remix';
	}

	public function rules()
	{

		return array(
			array('user_id, file_name, original_file_name', 'safe'),
			array('contest_id', 'required'),
			array('user_id', 'numerical', 'integerOnly' => true),
			array('file_name', 'length', 'max'=>100),

			array('author, contest, id, user_id, file_name', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'contest' => array(self::HAS_ONE, 'Contest',  array('id' => 'contest_id'), 'alias'=>'c'),
			'user'    => array(self::HAS_ONE, 'User', array('id' => 'user_id'), 'alias'=>'u' )
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' 		=> 'ID',
			'user_id' 	=> 'User',
			'file_name' => 'File Name',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('user', 'contest');
		$criteria->alias = 'r';
		$criteria->compare( 'id'       			, $this->id, true );
		$criteria->compare( 'user_id'  			, $this->user_id );
		$criteria->compare( 'file_name'			, $this->file_name ,true);
		$criteria->compare( 'u.first_name'		, $this->author, true);
		$criteria->compare( 'c.name'  			, $this->contest, true);

		$sort = new CSort();
		$sort->defaultOrder = 'r.created_on DESC';

		$sort->attributes['author'] = array( // добавляем сортировку по postTitle
			'sort'=>array(
				'attributes'=>array(
					'author'=>array(
						'asc'=>'u.first_name',
						'desc'=>'u.first_name DESC',
					),
					'contest'=>array(
						'asc'=>'c.name',
						'desc'=>'c.name DESC',
					),
					'*',
				),
			),
		);

		return new CActiveDataProvider( $this, array(
			'criteria' => $criteria,
			'sort'     => $sort
		));
	}

	public static function get_path($file_name)
	{
		return self::$upload_dir . $file_name;
	}

	public function get_create_on ()
	{
		return date('d F Y', strtotime($this->created_on) );
	}

}