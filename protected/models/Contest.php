<?php

/**
 * This is the model class for table "contests".
 *
 * The followings are the available columns in table 'contests':
 * @property integer $id
 * @property string $name
 * @property string $author
 * @property string $date_from
 * @property string $date_to
 * @property string $created
 * @property string $modified
 * @property string $description
 * @property string $more_requirements
 * @property string $more_label
 * @property string $prize
 * @property string $more_prize
 * @property string $vk
 * @property string $facebook
 * @property string $twitter
 * @property string $downloads
 * @property string $video
 * @property string $audio
 * @property string $web
 * @property string $slide
 */
class Contest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contests';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, date_from, date_to', 'required'),
			array('name,  vk, facebook, twitter, web, slide', 'length', 'max'=>255),
			array('title', 'length', 'max'=>100),

			array(
				'id, description_under,  name, author, date_from, date_to, created, modified, description,
				 cover, more_requirements,
				 more_label, prize, more_prize, vk, facebook, twitter, downloads, video, audio, web, slide',
				'safe'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => t('Name'),
            'author' => t('Author'),
			'date_from' => t('Date From'),
			'date_to' => t('Date To'),
			'created' => t('Created'),
			'modified' => t('Modified'),
			'description' => t('Description'),
			'description_under' => t('Description Second'),
			'more_requirements' => t('More Requirements'),
			'more_label' => t('More Label'),
			'prize' => t('Prize'),
			'more_prize' => t('More Prize'),
			'vk' => t('Vk'),
			'facebook' => t('Facebook'),
			'twitter' => t('Twitter'),
			'downloads' => t('Downloads'),
			'video' => t('Video'),
			'audio' => t('Audio'),
			'web' => t('Web'),
			'slide' => t('Slide'),
			'contest' => t('Contest'),
			'title' => 'Заголовок "О Конкурсе"'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date_from',$this->date_from,true);
		$criteria->compare('date_to',$this->date_to,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('more_requirements',$this->more_requirements,true);
		$criteria->compare('more_label',$this->more_label,true);
		$criteria->compare('prize',$this->prize,true);
		$criteria->compare('more_prize',$this->more_prize,true);
		$criteria->compare('vk',$this->vk,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('downloads',$this->downloads,true);
		$criteria->compare('video',$this->video,true);
		$criteria->compare('audio',$this->audio,true);
		$criteria->compare('web',$this->web,true);
		$criteria->compare('slide',$this->slide,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d H:i:s');
            }

            $this->modified = date('Y-m-d H:i:s');

            return true;
        }

        return false;
    }

	public function getStat()
	{
		$id = $this->id;
		$downloads = DowloadsStat::model()->findAll( 'contest_id = :cid', array(':cid' => $id ) );
		$remixes   = Remix::model()->findAll( 'contest_id = :cid', array(':cid' => $id));

		$download_stat = 0;
		$remixes_stat = 0;

		if (!empty($downloads)) {
			$download_stat = count($downloads);
		}

		if (!empty($remixes)) {
			$remixes_stat =  count($remixes);
		}

		echo '<div class="icon icon-arrow-down" title="Скачиваний">' . $download_stat .'</div>' .
			 '<div class="icon icon-arrow-up" title="Загрузок ремиксов">'. $remixes_stat . '</div>';

	}
}