<?php
/**
 * SuncloudOAuthService class file.
 *
 * Register application: http://vkontakte.ru/editapp?act=create&site=1
 * 
 * @author Andy B <jehkinen@ya.ru>
 * @link http://code.google.com/p/yii-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

require_once dirname(dirname(__FILE__)).'/EOAuth2Service.php';

/**
 * Suncloud provider class.
 * @package application.extensions.eauth.services
 */
class SoundCloudOAuthService extends EOAuth2Service {
	
	protected $name = 'suncloud';
	protected $title = 'soundcloud.com';
	protected $type = 'OAuth';
	protected $jsArguments = array('popup' => array('width' => 585, 'height' => 350));

	protected $client_id = '';
	protected $client_secret = '';
	protected $scope = 'non-expiring';
	protected $providerOptions = array(
		'authorize'    => 'https://soundcloud.com/connect',
		'access_token' => 'https://api.soundcloud.com/oauth2/token',
	);
	
	protected $uid = null;
	
	protected function fetchAttributes() {
		$info = (array)$this->makeSignedRequest('https://api.soundcloud.com/v1/me');
	}

	protected function getCodeUrl($redirect_uri) {
		$url = parent::getCodeUrl($redirect_uri);
		if (isset($_GET['js']))
			$url .= '&display=popup';
		return $url;
	}

	protected function saveAccessToken($token) {
		$this->setState('auth_token', $token->access_token);
		$this->setState('uid', $token->user_id);
		$this->setState('expires', time() + $token->expires_in - 60);
		$this->uid = $token->user_id;
		$this->access_token = $token->access_token;
	}

	protected function restoreAccessToken() {
		if ($this->hasState('uid') && parent::restoreAccessToken()) {
			$this->uid = $this->getState('uid');
			return true;
		}
		else {
			$this->uid = null;
			return false;
		}
	}

	protected function fetchJsonError($json) {
		if (isset($json->error)) {
			return array(
				'code' => $json->error->error_code,
				'message' => $json->error->error_msg,
			);
		}
		else
			return null;
	}
}