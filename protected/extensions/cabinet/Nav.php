<?php
Yii::import('bootstrap.widgets.TbMenu');

class Nav extends TbMenu
{
    /**
     * Renders the menu items.
     * @param array $items menu items. Each menu item will be an array with at least two elements: 'label' and 'active'.
     * It may have three other optional elements: 'items', 'linkOptions' and 'itemOptions'.
     */
    protected function renderMenu($items)
    {
        $n = count($items);

        if($n > 0)
        {
            echo CHtml::openTag('ul', $this->htmlOptions);

            $count = 0;
            foreach ($items as $item)
            {
                $count++;

                if (isset($item['divider']))
                    echo '<li class="'.$this->getDividerCssClass().'"></li>';
                else
                {
                    $options = isset($item['itemOptions']) ? $item['itemOptions'] : array();
                    $classes = array();

                    if ($item['active'] && $this->activeCssClass != '')  {
                        $classes[] = $this->activeCssClass;
                        if (isset($item['items']) && !empty($item['items'])) {
                            $classes[] = 'open';
                        }
                    }

                    if ($count === 1 && $this->firstItemCssClass !== null)
                        $classes[] = $this->firstItemCssClass;

                    if ($count === $n && $this->lastItemCssClass !== null)
                        $classes[] = $this->lastItemCssClass;

                    if ($this->itemCssClass !== null)
                        $classes[] = $this->itemCssClass;

                    if (isset($item['items']))
                        $classes[] = $this->getDropdownCssClass();

                    if (isset($item['disabled']))
                        $classes[] = 'disabled';

                    if (!empty($classes))
                    {
                        $classes = implode(' ', $classes);
                        if (!empty($options['class']))
                            $options['class'] .= ' '.$classes;
                        else
                            $options['class'] = $classes;
                    }

                    echo CHtml::openTag('li', $options);

                    $menu = $this->renderMenuItem($item);

                    if (isset($this->itemTemplate) || isset($item['template']))
                    {
                        $template = isset($item['template']) ? $item['template'] : $this->itemTemplate;
                        echo strtr($template, array('{menu}' => $menu));
                    }
                    else
                        echo $menu;

                    if (isset($item['items']) && !empty($item['items']))
                    {
                        $this->controller->widget('cabinet-widgets.Subnav', array(
                            'encodeLabel'=>$this->encodeLabel,
                            'htmlOptions'=>isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions,
                            'items'=>$item['items'],
                        ));
                    }

                    echo '</li>';
                }
            }

            echo '</ul>';
        }
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please see {@link items} on what data might be in the item.
     * @return string the rendered item
     */
    protected function renderMenuItem($item)
    {
        if (isset($item['icon']))
        {
            if (strpos($item['icon'], 'icon') === false)
            {
                $pieces = explode(' ', $item['icon']);
                $item['icon'] = 'icon-'.implode(' icon-', $pieces);
            }

            $item['label'] = '<i class="'.$item['icon'].'"></i><span class="menu-text">'.$item['label'].'</span>';
        }

        if (!isset($item['linkOptions']))
            $item['linkOptions'] = array();

        if (isset($item['items']) && !empty($item['items']))
        {
            $item['url'] = '#';

            if (isset($item['linkOptions']['class']))
                $item['linkOptions']['class'] .= ' dropdown-toggle';
            else
                $item['linkOptions']['class'] = 'dropdown-toggle';

            $item['label'] .= '<b class="arrow icon-angle-down"></b>';
        }

        if (isset($item['url']))
            return CHtml::link($item['label'], $item['url'], $item['linkOptions']);
        else
            return $item['label'];
    }

	/**
     * Returns the dropdown css class.
     * @return string the class name
     */
    public function getDropdownCssClass()
    {
        return '';
    }
}