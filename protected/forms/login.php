<?php
return array(
	'showErrorSummary' => true,
	'activeForm' => array(
		'class' => 'CActiveForm',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'id' => 'profile-form',
	),
	'elements' => array(
		'email' => array(
			'type' => 'text',

		),
		'password' => array(
			'type' => 'password',
			'maxlength' => 32,
		),
	),
	'buttons' => array(
		'login' => array(
			'type' => 'submit',
			'label' => t('Login'),
			'class' => 'btn btn-primary'
		)
	),
);