<?php

class m130810_185719_profile_lang extends CDbMigration
{
    public function safeUp()
    {
        $this->execute('alter table users add column language varchar(2)');
    }

    public function safeDown()
    {

    }
}